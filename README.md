# spring-h2

#### 项目介绍
h2内存数据库的使用

#### 软件架构
springboot spring-data-jpa h2


#### 安装教程

1. eclipse photon
2. springboot spring-data-jpa
3. com.h2database

#### 使用说明

1. 项目导入到eclipse中运行
2. 路径映射find，deleteAll，save，getByName？name=hw
3. 仅供学习和交流之用，请勿用作商业用途，使用之前请与我取得联系

#### 参与贡献

1. 何伟
2. 1807158216@qq.com
