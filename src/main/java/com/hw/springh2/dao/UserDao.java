package com.hw.springh2.dao;

import org.springframework.data.repository.CrudRepository;

import com.hw.springh2.model.User;

public interface UserDao extends CrudRepository<User, Long>{
    User findByName(String name);
}