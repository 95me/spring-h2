package com.hw.springh2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hw.springh2.dao.UserDao;
import com.hw.springh2.model.User;

@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping("save")
    public String save() {
    	for (int i = 0; i < 10; i++) {
    		User user = new User();
    		user.setName("user"+i);
    		user.setAge(i);
    		userDao.save(user);
		}

        return "users inserted";
    }

    @RequestMapping("find")
    public List<User> find() {
        return (List<User>) userDao.findAll();
    }
    
    @RequestMapping(value="findByName",method=RequestMethod.GET)
    public User findByName(@RequestParam("name") String name) {
        return userDao.findByName(name);
    }
    
    @RequestMapping("deleteAll")
    public String deleteAll() {
    	try {
    		userDao.deleteAll();
		} catch (Exception e) {
			return e.getMessage();
		}
    	return "ok";
    }
}